# FROM docker.io/library/golang:1.22.0 AS builder

# WORKDIR /app

# COPY . /app/

# RUN GOOS=linux GOARCH=amd64 go build -o go-app main.go

FROM docker.io/library/ubuntu:22.04

COPY ./build /usr/local/bin/go-app
ENTRYPOINT ["go-app"]
